using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using WindowRef = System.IntPtr;
using GDHandle = System.IntPtr;
using OSStatus = System.Int32;
using RgnHandle = System.IntPtr;
using HIWindowRef = System.IntPtr;
using EventRef = System.IntPtr;//Represents an opaque data structure that identifies individual events.

namespace Fireball.Carbon
{
    public class WindowManager
    {
        private const string LIB = "lib.dylib";

        /// <summary>
        /// Reports whether a pointer is a valid window pointer.
        /// </summary>
        /// <param name="possibleWindow"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern bool IsValidWindowPtr(WindowRef possibleWindow);

        /// <summary>
        /// Returns the graphics device with the greatest area of intersection with a specified window region.
        /// </summary>
        /// <param name="inWindow"></param>
        /// <param name="inRegion"></param>
        /// <param name="outGreatestDevice"></param>
        /// <param name="outGreatestDeviceRect"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern OSStatus GetWindowGreatestAreaDevice(WindowRef inWindow,
            WindowRegionCode inRegion, ref GDHandle outGreatestDevice,
            Rect outGreatestDeviceRect);

        /// <summary>
        /// Obtains a handle to a specific window region.
        /// </summary>
        /// <param name="inWindow"></param>
        /// <param name="inRegionCode"></param>
        /// <param name="ioWinRgn"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern OSStatus GetWindowRegion(WindowRef inWindow,
            WindowRegionCode inRegionCode, ref RgnHandle ioWinRgn);

        /// <summary>
        /// Obtains the window�s scale mode and the application�s display scale factor.
        /// </summary>
        /// <param name="inWindow"></param>
        /// <param name="outMode"></param>
        /// <param name="outScaleFactor"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern OSStatus HIWindowGetScaleMode(
           HIWindowRef inWindow,
           out HIWindowScaleMode outMode,
           out float outScaleFactor
           );

        /// <summary>
        /// Obtains the first window in a window list.
        /// </summary>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern WindowRef GetWindowList();

        /// <summary>
        /// Obtains the window part code of the window widget that is currently highlighted.
        /// </summary>
        /// <param name="inWindow"></param>
        /// <param name="outHilite"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern OSStatus GetWindowWidgetHilite(
           WindowRef inWindow,
           out WindowDefPartCode outHilite
           );

        /// <summary>
        /// Obtains the modification state of the specified window.
        /// </summary>
        /// <param name="window"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern bool IsWindowModified(WindowRef window);

        /// <summary>
        /// Sets the modification state of the specified window.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="modified"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern OSStatus SetWindowModified(WindowRef window, bool modified);

        /// <summary>
        /// Determines whether a Carbon event describing a click on a window�s title should cause a path
        /// selection menu to be displayed.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="inEvent"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern bool IsWindowPathSelectEvent(
                    WindowRef window,
                    EventRef inEvent
                    );

        /// <summary>
        /// Reports whether a mouse click should activate the window path pop-up menu.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="evt"></param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern bool IsWindowPathSelectClick(
                    WindowRef window,
                    [MarshalAs(UnmanagedType.LPStruct)]EventRecord evt
                    );

    }
}
