using System;
using System.Runtime.InteropServices;

namespace Fireball.Carbon
{
	[StructLayout(LayoutKind.Sequential)]
	public struct CGSize {
   		public float width;
   		public float height;
	}

    [StructLayout(LayoutKind.Sequential)]
    public struct Rect
    {
        public short top;
        public short left;
        public short bottom;
        public short right;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public short v;
        public short h;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CGRect
    {
        public CGPoint origin;
        public CGSize size;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CGPoint
    {
        public float x;
        public float y;
    }

    //Contains information associated with an event.
    [StructLayout(LayoutKind.Sequential)]
    public struct EventRecord
    {
        public EventKind what;
        public UInt32 message;
        public UInt32 when;
        public Point where;
        public EventModifiers modifiers;
    }

    [StructLayout(LayoutKind.Sequential)]
    public class CFDictionaryRef
    {

    }

}
