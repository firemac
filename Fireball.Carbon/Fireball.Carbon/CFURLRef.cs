using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Fireball.Carbon
{
    public class CFURLRef
    {
        private const string LIB = "/System/Library/Frameworks/Carbon.framework/Versions/Current/Carbon";

        [DllImport(LIB)]
        public static extern CFURLRef CFURLCreateWithString(
                   IntPtr allocator,
                   CFStringRef URLString,
                   CFURLRef baseURL
                );
    }
}
