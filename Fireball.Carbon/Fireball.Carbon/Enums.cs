using System;
using System.Runtime.InteropServices;

namespace Fireball.Carbon

{
    #region CGContext Enums

    public enum CGBlendMode
    {
        kCGBlendModeNormal,
        kCGBlendModeMultiply,
        kCGBlendModeScreen,
        kCGBlendModeOverlay,
        kCGBlendModeDarken,
        kCGBlendModeLighten,
        kCGBlendModeColorDodge,
        kCGBlendModeColorBurn,
        kCGBlendModeSoftLight,
        kCGBlendModeHardLight,
        kCGBlendModeDifference,
        kCGBlendModeExclusion,
        kCGBlendModeHue,
        kCGBlendModeSaturation,
        kCGBlendModeColor,
        kCGBlendModeLuminosity
    }

    public enum CGInterpolationQuality
    {
        kCGInterpolationDefault,
        kCGInterpolationNone,
        kCGInterpolationLow,
        kCGInterpolationHigh
    }

    public enum CGLineCap
    {
        kCGLineCapButt,
        kCGLineCapRound,
        kCGLineCapSquare
    }

    public enum CGLineJoin
    {
        kCGLineJoinMiter,
        kCGLineJoinRound,
        kCGLineJoinBevel
    }

    public enum CGColorRenderingIntent
    {
        kCGRenderingIntentDefault,
        kCGRenderingIntentAbsoluteColorimetric,
        kCGRenderingIntentRelativeColorimetric,
        kCGRenderingIntentPerceptual,
        kCGRenderingIntentSaturation
    }
    #endregion

    #region Windows Enums
    public enum WindowRegionCode : uint
    {
        kWindowTitleBarRgn = 0,
        kWindowTitleTextRgn = 1,
        kWindowCloseBoxRgn = 2,
        kWindowZoomBoxRgn = 3,
        kWindowDragRgn = 5,
        kWindowGrowRgn = 6,
        kWindowCollapseBoxRgn = 7,
        kWindowTitleProxyIconRgn = 8,
        kWindowStructureRgn = 32,
        kWindowContentRgn = 33,
        kWindowUpdateRgn = 34,
        kWindowOpaqueRgn = 35,
        kWindowGlobalPortRgn = 40,
        kWindowToolbarButtonRgn = 41
    }

    public enum HIWindowScaleMode : uint
    {
        kHIWindowScaleModeUnscaled = 0,
        kHIWindowScaleModeMagnified = 1,
        kHIWindowScaleModeFrameworkScaled = 2,
        kHIWinodwScaleModeApplicationScaled = 3
    }

    
    public enum WindowDefPartCode : uint
    {
        wNoHit = 0,
        wInContent = 1,
        wInDrag = 2,
        wInGrow = 3,
        wInGoAway = 4,
        wInZoomIn = 5,
        wInZoomOut = 6,
        wInCollapseBox = 9,
        wInProxyIcon = 10,
        wInToolbarButton = 11,
        wInStructure = 13
    }

    public enum EventKind
    {
        nullEvent = 0,
        mouseDown = 1,
        mouseUp = 2,
        keyDown = 3,
        keyUp = 4,
        autoKey = 5,
        updateEvt = 6,
        diskEvt = 7,
        activateEvt = 8,
        osEvt = 15,
        kHighLevelEvent = 23
    }

    public enum EventModifiers
    {
        activeFlagBit = 0,
        btnStateBit = 7,
        cmdKeyBit = 8,
        shiftKeyBit = 9,
        alphaLockBit = 10,
        optionKeyBit = 11,
        controlKeyBit = 12,
        rightShiftKeyBit = 13,
        rightOptionKeyBit = 14,
        rightControlKeyBit = 15
    }
    #endregion

    #region Strings enums
    public enum CFStringEncoding:int
    {
        kCFStringEncodingMacRoman = 0,
        kCFStringEncodingWindowsLatin1 = 0x0500,
        kCFStringEncodingISOLatin1 = 0x0201,
        kCFStringEncodingNextStepLatin = 0x0B01,
        kCFStringEncodingASCII = 0x0600,
        kCFStringEncodingUnicode = 0x0100,
        kCFStringEncodingUTF8 = 0x08000100,
        kCFStringEncodingNonLossyASCII = 0x0BFF,

        kCFStringEncodingUTF16 = 0x0100,
        kCFStringEncodingUTF16BE = 0x10000100,
        kCFStringEncodingUTF16LE = 0x14000100,
        kCFStringEncodingUTF32 = 0x0c000100,
        kCFStringEncodingUTF32BE = 0x18000100,
        kCFStringEncodingUTF32LE = 0x1c000100
    }
    #endregion
}
