using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Fireball.Carbon
{
    public unsafe class CFStringRef:ICloneable
    {
        #region Constants
        private const string LIB = "/System/Library/Frameworks/Carbon.framework/Versions/Current/Carbon";
        #endregion

        private IntPtr _Pointer = IntPtr.Zero;

        public IntPtr Pointer
        {
            get
            {
                return _Pointer;
            }
        }
        #region Apis
        /// <summary>
        /// 
        /// </summary>
        /// <param name="alloc">CFAllocatorRef</param>
        /// <param name="cStr"></param>
        /// <param name="encoding"></param>
        /// <returns>CFStringRef</returns>
 	[DllImport(LIB,CharSet = CharSet.Auto)]
        public static extern IntPtr /*CFStringRef*/CFStringCreateWithCString (
                   IntPtr alloc/*CFAllocatorRef*/,
                   string cStr,
                   CFStringEncoding encoding
                );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alloc">CFAllocatorRef</param>
        /// <param name="theString">CFStringRef pointer</param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern IntPtr CFStringCreateCopy (
                   IntPtr alloc,
                   IntPtr theString
                );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theString">CFStringRef</param>
        /// <param name="buffer"></param>
        /// <param name="bufferSize"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        [DllImport(LIB,CharSet=CharSet.Auto)]
        public static extern bool CFStringGetCString(
                   IntPtr theString,
                   StringBuilder buffer,
                   int bufferSize,
                   CFStringEncoding encoding
                );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theString">CFStringRef</param>
        /// <returns></returns>
        [DllImport(LIB)]
        public static extern Int32 CFStringGetLength(
                   IntPtr theString
                );

	[DllImport(LIB,CharSet=CharSet.Auto)]
		/*[return:MarshalAs(UnmanagedType.LPStr)]*/
	public static extern char* CFStringGetCStringPtr (
  		 IntPtr theString,
   		 CFStringEncoding encoding
		);  
	
	[DllImport(LIB)]
	public static extern CFStringEncoding CFStringGetFastestEncoding(IntPtr theString);

	[DllImport(LIB)]
	public static extern IntPtr CFStringCreateWithCStringNoCopy(IntPtr alloc,[MarshalAs(UnmanagedType.LPStr)]string s,CFStringEncoding encoding,IntPtr deAlloc);
        #endregion

	[DllImport(LIB)]
	public static extern CFStringEncoding CFStringGetSystemEncoding ();

	[DllImport(LIB)]
	public static extern void CFShowStr(IntPtr cfString);

        /// <summary>
        /// Return the value of CFStringRef as a standard .net String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            int size = CFStringGetLength(this.Pointer);

	    StringBuilder sb = new StringBuilder();
	    sb.EnsureCapacity(size+1);

            if(CFStringGetCString(this.Pointer,sb, size+1, CFStringEncoding.kCFStringEncodingMacRoman))
            	return sb.ToString();
	    else
		return null;
        }


        public CFStringRef(string s)
        {
		_Pointer = CFStringCreateWithCString(IntPtr.Zero, s,
  	              		CFStringEncoding.kCFStringEncodingMacRoman);
    	}

        internal CFStringRef(IntPtr cfstring)
        {
            _Pointer = cfstring;
        }


        #region ICloneable Members

        public object Clone()
        {
            IntPtr newPointer = CFStringCreateCopy(IntPtr.Zero,this.Pointer);

            CFStringRef cfStringRef = new CFStringRef(newPointer);

            return cfStringRef;                 
        }

        #endregion
    }
}
