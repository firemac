using System;
using System.Runtime.InteropServices;
using CFTypeID = System.UInt32;
using CGPatternRef = System.Int32;

namespace Fireball.Carbon
{
	public unsafe struct CGContextRef
	{
  		//Managing Graphics Contexts
        private const string LIB = "/System/Library/Frameworks/Carbon.framework/Versions/Current/Carbon";
	
 		[DllImport(LIB)]
		public static extern void CGContextFlush(CGContextRef* context);
	
		[DllImport(LIB)]
		public static extern CFTypeID  CGContextGetTypeID();

		[DllImport(LIB)]
		public static extern void CGContextRelease(CGContextRef* context);
	
		[DllImport(LIB)]
		public static extern CGContextRef CGContextRetain(CGContextRef* context);
	
		[DllImport(LIB)]
		public static extern void CGContextSynchronize(CGContextRef* context);

		//Saving and Restoring the Current Graphics State
	
		[DllImport(LIB)]
		public static extern void CGContextSaveGState(CGContextRef* context);

		[DllImport(LIB)]
		public static extern void CGContextRestoreGState(CGContextRef* context);

		//Getting and Setting Graphics State Parameters

		[DllImport(LIB)]
		public static extern CGInterpolationQuality CGContextGetInterpolationQuality(CGContextRef* context);

		[DllImport(LIB)]
		public static extern void CGContextSetFlatness(CGContextRef* context,float flatness);

		[DllImport(LIB)]
		public static extern void CGContextSetInterpolationQuality(CGContextRef* context,CGInterpolationQuality quality);
	
		[DllImport(LIB)]
		public static extern void CGContextSetLineCap(CGContextRef* context,CGLineCap cap);
	
		[DllImport(LIB)]
		public static extern void CGContextSetLineDash(CGContextRef* context,float phase, float[] lengths,uint count);

		[DllImport(LIB)]
		public static extern void CGContextSetLineJoin(CGContextRef* context, CGLineJoin join);

		[DllImport(LIB)]
		public static extern void CGContextSetLineWidth(CGContextRef* context, float width);

		[DllImport(LIB)]
		public static extern void CGContextSetMiterLimit(CGContextRef* context, float limit);

		[DllImport(LIB)]
		public static extern void CGContextSetPatternPhase(CGContextRef* context, CGSize phase);

	    [DllImport(LIB)]
		public static extern void CGContextSetFillPattern (
                       CGContextRef context,
                       CGPatternRef pattern,
                       float[] components);
                    

        [DllImport(LIB)]
		public static extern void CGContextSetRenderingIntent (
                   CGContextRef context,
                   CGColorRenderingIntent intent
                );

        [DllImport(LIB)]
        public static extern void CGContextSetShouldAntialias(
                   CGContextRef context,
                   bool shouldAntialias
                );
        [DllImport(LIB)]
        public static extern void CGContextSetShouldSmoothFonts(
                    CGContextRef context,
                    bool shouldSmoothFonts
                    );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="pattern"></param>
        /// <param name="components"></param>
        [DllImport(LIB)]
        public static extern void CGContextSetStrokePattern (
                   CGContextRef context,
                   CGPatternRef pattern,
                   float[] components
                );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mode"></param>
        [DllImport(LIB)]
        public static extern void CGContextSetBlendMode(
               CGContextRef context,
               CGBlendMode mode
            );

        /// <summary>
        /// Determines whether or not anti-aliasing can be turned on for a graphics context.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="allowsAntialiasing"></param>
        [DllImport(LIB)]
        public static extern void CGContextSetAllowsAntialiasing(
               CGContextRef context,
               bool allowsAntialiasing
            );

        /// <summary>
        /// Adds an arc of a circle to the current path, using a center point, radius, and end point.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        /// <param name="startAngle"></param>
        /// <param name="endAngle"></param>
        /// <param name="clockwise"></param>
        [DllImport(LIB)]
        public static extern void CGContextAddArc(
               CGContextRef context,
               float x,
               float y,
               float radius,
               float startAngle,
               float endAngle,
               int clockwise
            );

        [DllImport(LIB)]
        public static extern void CGContextAddArcToPoint(
               CGContextRef context,
               float x1,
               float y1,
               float x2,
               float y2,
               float radius
            );

        
       [DllImport(LIB)]
        public static extern IntPtr CGPDFContextCreateWithURL(
               [MarshalAs(UnmanagedType.LPStr)]string url,
               [MarshalAs(UnmanagedType.LPStruct)]CGRect   mediaBox,
              CFDictionaryRef auxiliaryInfo
            );


	}
}
